# Group7Project

## App User Instruction

Follow [this Google Doc](https://docs.google.com/document/d/1GzOz6lCIO9-j1esVCNZ64J18vl1_PMV1DgRqveJSo84/edit?usp=sharing) for a detailed cloning/downloading instructions!

## !Attention for Testers!

1. After cloning the project, please select ****Pixel 2 + upsideDownCake + landscape**** as your emulator setting! (detailed instructions above)
2. The game has great audio, so ****turning audio on**** is recommended, 
Wifi connection is required for the online leaderboard to work properly. All other parts do not. If you go to the leaderboard and nothing shows up, try going back and clicking sync data.
3. When you play the game for the first time, you receive a ****unique ID**** number that is binded to that device. You can change your username but the score would be updated to the same ID.
4. If you get sent back to the beginning of a game after finishing it - ****it is a glitch that we are experiencing****. Simply continue to the game and exit by hitting the pause button and “return to main menu.”


## Name

Project APEEP

## Presentation Materials

1. [Google Slides](https://docs.google.com/presentation/d/1aLUNfTfSxBmUgUYZ18YLy7gCx7WSqeTDpTB9A35sBtM/edit#slide=id.p) 
2. [Play through Video](https://www.youtube.com/watch?v=CVoGZUSaGrY)

## Description

Our game simulates different aspects of memorable moments from EC327, with an added twist of course!

## Members

Ankita, Ethan, Pippi, Elena, Pranav

## Contributions and Responsibilities

A - Ankita 100%
P - Pippi 100%
E - Elena 100%
E - Ethan 100%
P - Pranav 100%

Issue #7 - Personalize Gitlab has been completed
